(function (drupalSettings) {
  var firebaseConfig = drupalSettings.drupal_firebase.firebaseConfig;
  var config = {
    apiKey: firebaseConfig.apiKey,
    authDomain: firebaseConfig.authDomain,
    databaseURL: firebaseConfig.databaseURL,
    projectId: firebaseConfig.projectId,
    storageBucket: firebaseConfig.storageBucket,
    messagingSenderId: firebaseConfig.messagingSenderId,
    appId: firebaseConfig.appId
  };
  firebase.initializeApp(config);
})(drupalSettings);