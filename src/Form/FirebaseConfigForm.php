<?php

namespace Drupal\drupal_firebase\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FirebaseConfigForm.
 */
class FirebaseConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'drupal_firebase.firebaseconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'firebase_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('drupal_firebase.firebaseconfig');
    $form['service_account_key_file'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service account key file'),
      '#description' => $this->t('Download service account key json file and place it outside web directory of your project. Point to the file here as absoulte path or relative to your drupal project root.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('service_account_key_file'),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('API Key from apiKey key'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('api_key'),
    ];
    $form['auth_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auth Domain'),
      '#description' => $this->t('Auth Domain from authDomain key'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('auth_domain'),
    ];
    $form['database_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database URL'),
      '#description' => $this->t('Database URL'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('database_url'),
    ];
    $form['project_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project ID'),
      '#description' => $this->t('Project ID'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('project_id'),
    ];
    $form['storage_bucket'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Storage Bucket'),
      '#description' => $this->t('Storage Bucket'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('storage_bucket'),
    ];
    $form['messaging_sender_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Messaging Sender ID'),
      '#description' => $this->t('messagingSenderId'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('messaging_sender_id'),
    ];
    $form['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App ID'),
      '#description' => $this->t('appId'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('app_id'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('drupal_firebase.firebaseconfig')
      ->set('service_account_key_file', $form_state->getValue('service_account_key_file'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('auth_domain', $form_state->getValue('auth_domain'))
      ->set('database_url', $form_state->getValue('database_url'))
      ->set('project_id', $form_state->getValue('project_id'))
      ->set('storage_bucket', $form_state->getValue('storage_bucket'))
      ->set('messaging_sender_id', $form_state->getValue('messaging_sender_id'))
      ->set('app_id', $form_state->getValue('app_id'))
      ->save();
  }

}
