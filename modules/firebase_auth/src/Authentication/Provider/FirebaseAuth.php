<?php

namespace Drupal\firebase_auth\Authentication\Provider;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Authentication\AuthenticationProviderChallengeInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Http\Exception\CacheableUnauthorizedHttpException;
use Drupal\user\UserAuthInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Drupal\firebase_auth\AuthToken;
use Drupal\drupal_firebase_users\FirebaseTokenManager;
use Drupal\drupal_firebase_users\DrupalUserBridge;

/**
 * Firebase authentication provider.
 */
class FirebaseAuth implements AuthenticationProviderInterface, AuthenticationProviderChallengeInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The user auth service.
   *
   * @var \Drupal\user\UserAuthInterface
   */
  protected $userAuth;

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Firebase token manager service.
   *
   * @var \Drupal\drupal_firebase_users\FirebaseTokenManager
   */
  protected $firebaseTokenManager;

  /**
   * The AuthToken helper.
   *
   * @var \Drupal\firebase_auth\AuthToken;
   */

  /**
   * Constructs a Firebase authentication provider object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\user\UserAuthInterface $user_auth
   *   The user authentication service.
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, UserAuthInterface $user_auth, FloodInterface $flood, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->userAuth = $user_auth;
    $this->flood = $flood;
    $this->entityTypeManager = $entity_type_manager;

    $this->firebaseTokenManager = new FirebaseTokenManager();
    $this->authToken = new AuthToken();
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request) {
    $token = $this->authToken->getBearerToken();
    return isset($token);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request) {
    $flood_config = $this->configFactory->get('user.flood');
    $token = $this->authToken->getBearerToken();
    $drupalUserBridge = new DrupalUserBridge();

    $firebase_user = $this->firebaseTokenManager->verifyToken($token);
    if ($firebase_user == NULL) {
      return NULL;
    }
    $username = $firebase_user->email;
    $drupal_user = $drupalUserBridge->getDrupalUserForFirebaseUser($firebase_user->uid);
    return $this->entityTypeManager->getStorage('user')->load($drupal_user->id());
  }

  /**
   * {@inheritdoc}
   */
  public function challengeException(Request $request, \Exception $previous) {
    $site_config = $this->configFactory->get('system.site');
    $site_name = $site_config->get('name');
    $challenge = new FormattableMarkup('Basic realm="@realm"', [
      '@realm' => !empty($site_name) ? $site_name : 'Access restricted',
    ]);

    // A 403 is converted to a 401 here, but it doesn't matter what the
    // cacheability was of the 403 exception: what matters here is that
    // authentication credentials are missing, i.e. this request was made
    // as an anonymous user.
    // Therefore, the following actions will be taken:
    // 1. Verify whether the current user has the 'anonymous' role or not. This
    //    works fine because:
    //    - Thanks to \Drupal\firebase_auth\PageCache\DisallowFirebaseAuthRequests,
    //      Page Cache never caches a response whose request has Firebase Auth
    //      credentials.
    //    - Dynamic Page Cache will cache a different result for when the
    //      request is unauthenticated (this 401) versus authenticated (some
    //      other response)
    // 2. Have the 'config:user.role.anonymous' cache tag, because the only
    //    reason this 401 would no longer be a 401 is if permissions for the
    //    'anonymous' role change, causing the cache tag to be invalidated.
    // @see \Drupal\Core\EventSubscriber\AuthenticationSubscriber::onExceptionSendChallenge()
    // @see \Drupal\Core\EventSubscriber\ClientErrorResponseSubscriber()
    // @see \Drupal\Core\EventSubscriber\FinishResponseSubscriber::onAllResponds()
    $cacheability = CacheableMetadata::createFromObject($site_config)
      ->addCacheTags(['config:user.role.anonymous'])
      ->addCacheContexts(['user.roles:anonymous']);
    return $request->isMethodCacheable()
      ? new CacheableUnauthorizedHttpException($cacheability, (string) $challenge, 'No authentication credentials provided.', $previous)
      : new UnauthorizedHttpException((string) $challenge, 'No authentication credentials provided.', $previous);
  }

}
