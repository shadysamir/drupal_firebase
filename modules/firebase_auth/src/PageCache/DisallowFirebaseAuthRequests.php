<?php

namespace Drupal\firebase_auth\PageCache;

use Drupal\Core\PageCache\RequestPolicyInterface;
use Symfony\Component\HttpFoundation\Request;

use Drupal\firebase_auth\AuthToken;

/**
 * Cache policy for pages served from firebase auth.
 *
 * This policy disallows caching of requests that use firebase_auth for security
 * reasons. Otherwise responses for authenticated requests can get into the
 * page cache and could be delivered to unprivileged users.
 */
class DisallowFirebaseAuthRequests implements RequestPolicyInterface {

  /**
   * {@inheritdoc}
   */
  public function check(Request $request) {
    $auth_token = new AuthToken();
    $token = $auth_token->getBearerToken();
    if (isset($token)) {
      return self::DENY;
    }
  }

}
