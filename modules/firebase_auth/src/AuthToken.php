<?php

namespace Drupal\firebase_auth;

/**
 * Helper class to extract auth token from headers.
 */
class AuthToken {

  /**
   * Get header Authorization.
   */
  protected function getAuthorizationHeader() {
    $headers = NULL;
    if (isset($_SERVER['Authorization'])) {
      $headers = trim($_SERVER["Authorization"]);
    }
    elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) {
      // Nginx or fast CGI.
      $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
    }
    elseif (function_exists('apache_request_headers')) {
      $requestHeaders = apache_request_headers();
      // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
      $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
      if (isset($requestHeaders['Authorization'])) {
        $headers = trim($requestHeaders['Authorization']);
      }
    }
    return $headers;
  }

  /**
   * Get access token from header.
   * */
  public function getBearerToken() {
    $header = $this->getAuthorizationHeader();
    // HEADER: Get the access token from the header.
    if (!empty($header)) {
      if (str_starts_with($header, "Bearer")) {
        $token = substr($header, 7);
        return $token;
      }
    }
    return NULL;
  }

}
