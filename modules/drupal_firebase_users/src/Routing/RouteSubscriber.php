<?php

namespace Drupal\drupal_firebase_users\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('user.login')) {
      $route->setDefaults([
        '_controller' => '\Drupal\drupal_firebase_users\Controller\SignInController::content',
      ]);
    }
    if ($route = $collection->get('user.register')) {
      $route->setRequirement('_access', 'FALSE');
    }
    if ($route = $collection->get('user.pass')) {
      $route->setRequirement('_access', 'FALSE');
    }
    if ($route = $collection->get('user.reset.login')) {
      $route->setRequirement('_access', 'FALSE');
    }
  }

}
