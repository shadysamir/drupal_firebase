<?php

namespace Drupal\drupal_firebase_users\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class SignInController.
 */
class SignInController extends ControllerBase {

  /**
   * Content.
   *
   * @return string
   *   Return sign in screen.
   */
  public function content() {
    return [
      '#type' => 'markup',
      '#markup' => '<div id="firebaseui-auth-container"></div>',
      '#attached' => [
        'library' => [
          'drupal_firebase_users/sign-in',
        ],
      ],
    ];
  }

}
