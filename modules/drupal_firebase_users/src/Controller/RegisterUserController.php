<?php

namespace Drupal\drupal_firebase_users\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\core\Database\Database;
use Drupal\user\Entity\User;
use function GuzzleHttp\json_decode;
use Drupal\Core\Url;
use Kreait\Firebase\Auth\UserRecord;
use League\Flysystem\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\drupal_firebase_users\FirebaseTokenManager;
use Drupal\drupal_firebase_users\DrupalUserBridge;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Kreait\Firebase\Auth\SendActionLink\FailedToSendActionLink;

/**
 * Class RegisterUserController.
 */
class RegisterUserController extends ControllerBase {
  public function __construct() {
    $this->firebaseTokenManager = new FirebaseTokenManager();
    $this->drupalUserBridge = new DrupalUserBridge();
  }

  /**
   * Token manager.
   *
   * @var \Drupal\drupal_firebase_users\FirebaseTokenManager
   */
  private $firebaseTokenManager;

  /**
   * Drupal User Bridge.
   *
   * @var \Drupal\drupal_firebase_users\DrupalUserBridge
   */
  private $drupalUserBridge;

  /**
   * Register.
   *
   * @return Drupal\drupal_firebase_users\Controller\JsonResponse
   *   Return Hello string.
   */
  public function register(Request $request): JsonResponse {
    $messenger = \Drupal::messenger();
    $user_tokens = json_decode($request->getContent());
    // Check that the token and userid were sent.
    if (!property_exists($user_tokens, 'idToken') || !property_exists($user_tokens, 'userId')) {
      return new JsonResponse('Both idToken and userId values are required.', 400);
    }

    // Verify the token with firebase.
    $firebase_user = $this->firebaseTokenManager->verifyToken($user_tokens->idToken);
    if ($firebase_user == NULL) {
      return new JsonResponse('Invalid Token', 403);
    }
    // Match the userid from the token with the userid from request
    // as an extra security measure.
    if ($firebase_user->uid != $user_tokens->userId) {
      // Revoke token user id.
      $this->firebaseTokenManager->revokeToken($firebase_user->uid);
      return new JsonResponse('User ID does not match token', 403);
    }
    // Try to find an existing drupal user registered
    // with drupal_firebase_users module.
    $drupal_user = $this->drupalUserBridge->getDrupalUserForFirebaseUser($firebase_user->uid);
    if (isset($drupal_user)) {
      if (!$firebase_user->emailVerified) {
        $messenger->addWarning(t("Please verify your email address by following the verification link sent to @email", ['@email' => $firebase_user->email]));
      }
      $this->updateVerifiedRole($firebase_user, $drupal_user, FALSE);
      user_login_finalize($drupal_user);
      return new JsonResponse(
        [
          'success' => TRUE,
        ]
      );
    }
    // Try to find an existing drupal user with the same email to link.
    $users = \Drupal::entityTypeManager()->getStorage('user')
      ->loadByProperties(['mail' => $firebase_user->email]);
    $user = reset($users);
    if (!$user) {
      // Create a new drupal user for this firebase user.
      $user = User::create();
      $user->enforceIsNew();
      $user->setEmail($firebase_user->email);
      $user->setUsername($firebase_user->email);
      $user->activate();
      $user->save();
    }
    $drupal_uid = $user->id();
    try {
      $result = Database::getConnection()->merge('drupal_firebase_users_user_map')
        ->key('uid', $drupal_uid)
        ->fields([
          'firebase_uid' => $firebase_user->uid,
        ])
        ->execute();
    }
    catch (Exception $e) {
      return new JsonResponse('Cannot link to existing Drupal user', 400);
    }
    $drupal_user = User::load($drupal_uid);
    if (!$firebase_user->emailVerified) {
      $continueUrl = Url::fromRoute(
        'drupal_firebase_users.finish_email_verification',
        [],
        [
          'absolute' => TRUE,
          'query' => [
            'final_destination' => Url::fromRoute('almanassa.userprofile', [], ['absolute' => TRUE])->toString(),
            'uid' => $drupal_uid,
            'fbuid' => $firebase_user->uid,
          ],
        ],
      )->toString();
      $this->firebaseTokenManager->firebaseInstance()->createAuth()->sendEmailVerificationLink($firebase_user->email,
        [
          'continueUrl' => $continueUrl,
        ],
        'ar',
      );
      $messenger->addWarning(t("Please verify your email address by following the verification link sent to @email", ['@email' => $firebase_user->email]));
    }
    else {
      $this->updateVerifiedRole($firebase_user, $drupal_user, FALSE);
    }
    user_login_finalize($drupal_user);
    return new JsonResponse(
      [
        'uid' => $drupal_uid,
      ]
    );
  }

  public function finishEmailVerification(Request $request) {
    $uid = $request->query->get('uid');
    $fbuid = $request->query->get('fbuid');
    $firebase_user = $this->firebaseTokenManager->firebaseInstance()->createAuth()->getUser($fbuid);
    $drupal_user = User::load($uid);
    $this->updateVerifiedRole($firebase_user, $drupal_user, FALSE);
    $final_destination = $request->query->get('final_destination');
    $response = new RedirectResponse($final_destination ?? Url::fromRoute('<front>'));
    return $response;
  }

  public function sendVerificationEmail(Request $request) {
    $user = \Drupal::currentUser();
    $firebase_user = $this->drupalUserBridge->getFirebaseUserForDrupalUser($user);
    if ($firebase_user != NULL) {
      try {
        $this->firebaseTokenManager->firebaseInstance()->createAuth()->sendEmailVerificationLink($firebase_user->email,
          [
            'continueUrl' => Url::fromRoute('drupal_firebase_users.finish_email_verification', [], ['absolute' => TRUE])->toString(),
          ],
          'ar',
        );
      }
      catch (FailedToSendActionLink $th) {
        if ($th->getMessage() == 'TOO_MANY_ATTEMPTS_TRY_LATER') {
          \Drupal::messenger()->addError(t('Too many attempts to send verificvation email. Please try again in a while.'));
        }
      }
    }
    $final_destination = $request->query->get('destination') ?? $request->headers->get('referer');
    $response = new RedirectResponse($final_destination ?? Url::fromRoute('<front>')->toString());
    return $response;
  }

  /**
   * Custom access control for email verification path.
   */
  public function sendVerificationEmailAccess(AccountInterface $account) {
    // Check permissions and combine that with any custom access checking needed. Pass forward
    // parameters from the route and/or request as needed.
    return AccessResult::allowedIf(!in_array('verified', $account->getRoles()));
  }

  /**
   * Assign verified user to user with verified email.
   */
  private function updateVerifiedRole(UserRecord $firebase_user, User $drupal_user, bool $send_email) {
    if ($firebase_user->emailVerified && $drupal_user->id() != 1 && !in_array('verified', $drupal_user->getRoles())) {
      $drupal_user->addRole('verified');
      $drupal_user->save();
      \Drupal::moduleHandler()->invokeAll('drupal_firebase_users_post_verification', [$drupal_user]);
    }
    elseif (!$firebase_user->emailVerified && in_array('verified', $drupal_user->getRoles())) {
      $drupal_user->removeRole('verified');
      $drupal_user->save();
    }
  }

  /**
   * Method to fire not found exception.
   */
  public function notfound() {
    throw new NotFoundHttpException();
  }

}
