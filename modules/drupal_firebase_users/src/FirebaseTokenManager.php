<?php

namespace Drupal\drupal_firebase_users;

use Kreait\Firebase\Factory;
use Firebase\Auth\Token\Exception\InvalidToken;

/**
 * Firebase token manager handles all auth token operations with Firebase.
 */
class FirebaseTokenManager {

  /**
   * Constructor function initiates Firebase.
   */
  public function __construct() {
    $firebaseconfig = \Drupal::config('drupal_firebase.firebaseconfig');
    $service_account_key_file = $firebaseconfig->get('service_account_key_file');
    if (!str_starts_with($service_account_key_file, "/")) {
      $service_account_key_file = DRUPAL_ROOT . '/' . $service_account_key_file;
    }

    // Initiate firebase factory.
    $this->firebase = (new Factory)
      ->withServiceAccount($service_account_key_file);
  }

  /**
   * Get the firebase factory.
   *
   * @return \Kreait\Firebase\Factory
   *   The firebase factory instance.
   */
  public function firebaseInstance() {
    return $this->firebase;
  }

  /**
   * Firebase factory to be used in operations.
   *
   * @var \Kreait\Firebase\Factory
   */
  private $firebase;

  /**
   * Verify a token with Firebase and resturns Firebase uid.
   *
   * @return \Kreait\Firebase\Auth\UserRecord
   *   The UserRecord related to the verified token, or NULL if not verified.
   */
  public function verifyToken(string $token) {
    // Verify the token with firebase.
    try {
      $verifiedIdToken = $this->firebase->createAuth()->verifyIdToken($token);
    }
    catch (InvalidToken $e) {
      return NULL;
    }
    // Get userid for that token.
    $firebase_uid = $verifiedIdToken->claims()->get('sub');

    // Get the firebase user object.
    $firebase_user = $this->firebase->createAuth()->getUser($firebase_uid);
    return $firebase_user;
  }

  /**
   * Revoke token for a uid.
   */
  public function revokeToken(string $uid) {
    $this->firebase->createAuth()->revokeRefreshTokens($uid);
  }

}
