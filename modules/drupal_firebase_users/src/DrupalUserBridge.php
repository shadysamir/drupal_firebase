<?php

namespace Drupal\drupal_firebase_users;

use Drupal\core\Database\Database;
use Drupal\user\Entity\User;
use Drupal\drupal_firebase_users\FirebaseTokenManager;
use Kreait\Firebase\Auth\UserRecord;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Class to bridge between Drupal users and Firebase users.
 */
class DrupalUserBridge {

  /**
   * Token manager.
   *
   * @var \Drupal\drupal_firebase_users\FirebaseTokenManager
   */
  private $firebaseTokenManager;

  public function __construct() {
    $this->firebaseTokenManager = new FirebaseTokenManager();
  }

  /**
   * Get Drupal user by Firebase user id.
   *
   * @param string $uid
   *   Firebase user id.
   *
   * @return \Drupal\user\Entity\User
   *   Drupal user.
   */
  public function getDrupalUserForFirebaseUser(string $uid) {
    // Try to find an existing drupal user registered
    // with drupal_firebase_users module.
    $select = Database::getConnection()->select('drupal_firebase_users_user_map', 'f');
    $select->join('users_field_data', 'u', 'f.uid = u.uid');
    $select->addField('u', 'uid');
    $select->condition('f.firebase_uid', $uid);
    $result = $select->execute();
    $user_row = $result->fetchObject();
    if (!empty($user_row)) {
      $drupal_uid = $user_row->uid;
      $drupal_user = User::load($drupal_uid);
      return $drupal_user;
    }
    return NULL;
  }

  /**
   * Return Firebase user based on drupal uid.
   *
   * @return \Kreait\Firebase\Auth\UserRecord
   *   The UserRecord for this firebase user.
   */
  public function getFirebaseUserForDrupalUser(AccountProxyInterface $drupal_user): ?UserRecord {
    // Try to find an existing drupal user registered
    // with drupal_firebase_users module.
    $select = Database::getConnection()->select('drupal_firebase_users_user_map', 'f');
    $select->addField('f', 'firebase_uid');
    $select->condition('f.uid', $drupal_user->id());
    $result = $select->execute();
    $user_row = $result->fetchObject();
    if (!empty($user_row)) {
      $firebase_uid = $user_row->firebase_uid;
      $firebase_user = $this->firebaseTokenManager->firebaseInstance()->createAuth()->getUser($firebase_uid);
      return $firebase_user;
    }
    return NULL;
  }

}
