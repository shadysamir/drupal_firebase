<?php

namespace Drupal\drupal_firebase_users\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the drupal_firebase_users module.
 */
class RegisterUserControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "drupal_firebase_users RegisterUserController's controller functionality",
      'description' => 'Test Unit for module drupal_firebase_users and controller RegisterUserController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests drupal_firebase_users functionality.
   */
  public function testRegisterUserController() {
    // Check that the basic functions of module drupal_firebase_users.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
