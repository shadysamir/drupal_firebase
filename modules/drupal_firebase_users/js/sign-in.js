(function (drupalSettings, $) {
  let destination = new URLSearchParams(window.location.search).get(
    "destination"
  );
  const signInSuccessUrl = destination || drupalSettings.path.baseUrl;
  var uiConfig = {
    signInSuccessUrl,
    signInFlow: "popup",
    signInOptions: [
      {
        provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
        requireDisplayName: false,
      },
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    ],
    credentialHelper: firebaseui.auth.CredentialHelper.NONE,
    tosUrl: drupalSettings.path.baseUrl,
    // Privacy policy url/callback.
    privacyPolicyUrl: function () {
      window.location.assign(drupalSettings.path.baseUrl);
    },
    callbacks: {
      signInSuccessWithAuthResult: (authResult) =>
        signIntoDrupal(authResult, signInSuccessUrl),
      signInFailure: function (error) {
        // Some unrecoverable error occurred during sign-in.
        // Return a promise when error handling is completed and FirebaseUI
        // will reset, clearing any UI. This commonly occurs for error code
        // 'firebaseui/anonymous-upgrade-merge-conflict' when merge conflict
        // occurs. Check below for more details on this.
        return handleUIError(error);
      },
    },
  };

  // Initialize the FirebaseUI Widget using Firebase.
  var ui = new firebaseui.auth.AuthUI(firebase.auth());
  // Check if a firebase user is already logged in, show ui omly for anonymous
  if (!firebase.auth().currentUser) {
    // The start method will wait until the DOM is loaded.
    ui.start("#firebaseui-auth-container", uiConfig);
  }

  function signIntoDrupal(authResult, redirectPath) {
    firebase
      .auth()
      .currentUser.getIdToken(true)
      .then(function (idToken) {
        var userTokens = {
          userId: authResult.user.uid,
          idToken: idToken,
        };
        $.post(
          drupalSettings.path.baseUrl + "drupal_firebase_users/register",
          JSON.stringify(userTokens)
        )
          .done(function (data) {
            console.log(data);
            window.location.href = redirectPath
          })
          .fail(function (error) {
            console.log(error.responseJSON);
            // unable to sign into Drupal. Need to sign out from firebase as well
            firebase.auth().signOut();
          });
      })
      .catch(function (error) {
        console.log(error);
      });

    return false;
  }
})(drupalSettings, jQuery);
